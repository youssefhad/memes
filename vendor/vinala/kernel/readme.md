
## Vinala Kernel

[![StyleCI](https://styleci.io/repos/71659585/shield?branch=master)](https://styleci.io/repos/71659585)
[![Latest Stable Version](https://poser.pugx.org/vinala/kernel/v/stable)](https://packagist.org/packages/vinala/kernel) [![Total Downloads](https://poser.pugx.org/vinala/kernel/downloads)](https://packagist.org/packages/vinala/kernel) [![Latest Unstable Version](https://poser.pugx.org/vinala/kernel/v/unstable)](https://packagist.org/packages/vinala/kernel) 
[![License](https://poser.pugx.org/vinala/kernel/license)](https://packagist.org/packages/vinala/kernel)

-------

This repository contains the core code of the Vinala framework

### Licence

The Vinala kernel is **not** open-sourced software, it's personal project of Youssef Had

[![Owner](https://img.shields.io/badge/created%20by-Youssef%20Had-blue.svg)](https://www.facebook.com/yussef.had)
[![Owner](https://img.shields.io/badge/copyright-2014--2017-red.svg)](https://github.com/vinala/kernel)
[![Owner](https://img.shields.io/badge/launched-10%2F10%2F2014-ff2f6c.svg)](https://github.com/vinala/kernel)